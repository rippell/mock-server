Mock Server
-----------

Super simple mock server that allows applications to be built as if hitting a real server because having to make code changes to run locally vs in production isn't cool.

Goals are to:
* Keep code and mocks separated
  * No code changes for mocking and mocks aren't bundled with code
* Keep mocks and data close to the code
  * Clean organization and not a jumbled mess of spaghetti files
* Allow for extensible mocking of responses and statuses
  * Give open access to the underlying server without complications
* Live-reload!
  * Keep up with fast paced development by refreshing mocks when files are changed

## Install

Install as a dev dependency, along with `npm-run-all` to run in parrallel with your code development server.
```
npm i --save-dev @common/mock-server npm-run-all
```

Create a proxy configuration file at the top of the project to proxy all HTTP calls to the mock server (`proxy.mock.json`)

```json
{
  "/api": {
    "target": "http://localhost:4300",
    "secure": false,
    "debug": true
  }
}
```

Change start command to start both servers:

```json
{
    ...
    "start": "npm-run-all --parallel mock-server start-proxy-mocks",
    "start-proxy-mocks": "ng serve --proxy-config proxy.mock.json",
    "mock-server": "mock-server",
    ...
}
```

## Basic Usage

When booting up, a file scan is done to discover controllers - `*.mock.js` files.
It's recommended to keep the mock file with the service it belongs to.
By default the `Mock Server` will search `/src/` for all files matching `*.mock.js` to register with the server.

```
my-project
 |- node_modules
 |- src
     |- app
         |- services
             |- my-service
                 |- myService.service.ts
                 |- myService.mock.js
                 |- myService.data.json
```

`myService.mock.js`
```
module.exports = {
  '/api/v1/endpoint': {
    get: (req, res) => {
      res.json({ msg: 'Hurrah!'});
    }
  }
}
```

---

If mock data is large, it's also common to create a `.json` file and include it into the `*.mock.js`.

`myService.data.json`
```
{
    "mySubObject": {
        "msg": "Sweet response!"
    }
}
```

`myService.mock.js`
```
var data = require('./my-data.json');

module.exports = {
  '/api/v1/endpoint': {
    get: (req, res) => {
      res.json(data.mySubObject);
    }
  }
}
```