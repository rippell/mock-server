const path = require('path'),
      dir = process.cwd();

const mockPattern = process.env.MOCK_PATTERN || process.argv[3] || 'src/**/*.mock.js';

module.exports = {
    dir: dir,
    mockPattern: path.join(path.resolve('./'), mockPattern),
    host: process.env.MOCK_HOST || '0.0.0.0',
    port: process.env.MOCK_Port || process.argv[2] || '4300',
    verbose: process.env.MOCK_VERBOSE || process.argv[4] || false
};
