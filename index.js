#!/usr/bin/env node --inspect

const Server = require('./server'),
    config = require('./config'),
    globwatcher = require('globwatcher').globwatcher,
    server = new Server(),
    watcher = globwatcher(config.mockPattern),

    update = (controller) => {
        delete require.cache[controller];
        server.updateRoutes();
    },

    watch = () => {
        watcher.on("added", update);
        watcher.on("changed", update);
        watcher.on("deleted", update);

        watcher.ready.then(() => console.debug("Watching for changes!\n"));
    };

server.start(watch);

module.exports = server;
