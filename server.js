"use strict";

const express = require('express'),
      http = require('http'),
      cors = require('cors'),
      helmet = require('helmet'),
      glob = require('glob'),
      compression = require('compression'),
      bodyParser = require('body-parser'),
      config = require('./config'),
      log = config.verbose ? console.debug : ()=>{};

module.exports = class Server {
  constructor() {
      this.app = null;
  }

  start(cb){
      this.updateRoutes(true, cb);
  }

  bootstrapServer() {
    this.app = express();
    this.app.use(cors({
        origin: (o, cb) => cb(null, true),
        credentials: true
    }));
    this.app.use(helmet());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: true}));
    this.app.use(compression());

    this.app.use((req, res, next) => {
        log(`Call to ${req.url}`);
        this.router(req, res, next);
    });

    this.app.get('*', (req, res) => {
        const msg = `[WARN] No controller endpoint for ${req.url}`;
        log(msg);
        res.status(404).send({error: msg});
    });

    this.httpServer = http.createServer(this.app)
        .listen(config.port, config.host, () => log(`Listening on ${config.host}:${config.port}`));
  }

  updateRoutes(bootstrap, cb) {
      glob(config.mockPattern, {cache: false}, (er, mocks) => {
          log('\nStarting route discovery\n');
          this.router = express.Router();

          mocks.forEach(mock => {
              log(mock.replace(config.dir, ''));

              try {
                  Object.entries(require(mock)).forEach(([url, methods]) =>{
                      log(`  --> ${url}`);

                      Object.entries(methods).forEach(([method, func]) => {
                        log(`    --> ${method}`);
                          this.router[method](url,func);
                      })
                  })
              } catch (e) {
                  console.error(`\n[ERR] File error:`);
                  console.error(e);
                  console.error('');
              }
              log('');
          });

          log('Finished route registration\n');

          if(bootstrap) {
              this.bootstrapServer();
          }
          if (cb) {
              cb(mocks);
          }
      });
  }
};
